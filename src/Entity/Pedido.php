<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PedidoRepository")
 */
class Pedido
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $endereco;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $cep;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $cidade;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $uf;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $valor = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $qtd = 0;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PedidoItem", mappedBy="pedido")
     */
    private $itens;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $bairro;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $formaPgto;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telefone;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $numero;

    public function __construct()
    {
        $this->itens = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(?string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getEndereco(): ?string
    {
        return $this->endereco;
    }

    public function setEndereco(?string $endereco): self
    {
        $this->endereco = $endereco;

        return $this;
    }

    public function getCep(): ?string
    {
        return $this->cep;
    }

    public function setCep(?string $cep): self
    {
        $this->cep = $cep;

        return $this;
    }

    public function getCidade(): ?string
    {
        return $this->cidade;
    }

    public function setCidade(?string $cidade): self
    {
        $this->cidade = $cidade;

        return $this;
    }

    public function getUf(): ?string
    {
        return $this->uf;
    }

    public function setUf(?string $uf): self
    {
        $this->uf = $uf;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getValor(): ?float
    {
        return $this->valor;
    }

    public function setValor(?float $valor): self
    {
        $this->valor = $valor;

        return $this;
    }

    public function getQtd(): ?int
    {
        return $this->qtd;
    }

    public function setQtd(?int $qtd): self
    {
        $this->qtd = $qtd;

        return $this;
    }

    /**
     * @return Collection|PedidoItem[]
     */
    public function getItens(): Collection
    {
        return $this->itens;
    }

    public function addItem(PedidoItem $item): self
    {
        if (!$this->itens->contains($item)) {
            $this->itens[] = $item;
            $this->qtd++;
            $this->valor += ($item->getQuantidade() * $item->getProduto()->getValor());
            $item->setPedido($this);
        }

        return $this;
    }

    public function removeItem(PedidoItem $item): self
    {
        if ($this->itens->contains($item)) {
            $this->itens->removeElement($item);
            $this->qtd--;
            $this->valor -= ($item->getQuantidade() * $item->getProduto()->getValor());
            // set the owning side to null (unless already changed)
            if ($item->getPedido() === $this) {
                $item->setPedido(null);
            }
        }

        return $this;
    }

    public function getBairro(): ?string
    {
        return $this->bairro;
    }

    public function setBairro(?string $bairro): self
    {
        $this->bairro = $bairro;

        return $this;
    }

    public function getFormaPgto(): ?string
    {
        return $this->formaPgto;
    }

    public function setFormaPgto(string $formaPgto): self
    {
        $this->formaPgto = $formaPgto;

        return $this;
    }

    public function getTelefone(): ?string
    {
        return $this->telefone;
    }

    public function setTelefone(?string $telefone): self
    {
        $this->telefone = $telefone;

        return $this;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(?string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }
}
