<?php
namespace App\DataFixtures;

use App\Entity\Produto;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     *  Carrega todos os produtos na base de dados
     */
    public function load(ObjectManager $manager)
    {
        $produtos = [
            [
                'desc' => 'Fone de Ouvido Bluetooth Pulse PH215/6 Multilaser',
                'img' => '/images/prod0.jpg',
                'preco' => 219.9
            ],
            [
                'desc' => 'SSD Samsung 970 Pro M.2 NVMe 1TB',
                'img' => '/images/prod1.jpg',
                'preco' => 2268.39
            ],
            [
                'desc' => 'Pen Drive Kingston DataTraveler USB 3.0 16GB - DT100G3/16GB',
                'img' => '/images/prod2.jpg',
                'preco' => 22.9
            ],
            [
                'desc' => 'Memória Corsair Vengeance LPX 8GB 2666Mhz DDR4 CL16 Red - CMK8GX4M1A2666C16R',
                'img' => '/images/prod3.jpg',
                'preco' => 458.71
            ],
            [
                'desc' => 'Roteador D-Link 300Mbps, Dual Antena 5dBi, IPv6, Modo Repetidor, Bivolt, Preto - DIR-615',
                'img' => '/images/prod4.jpg',
                'preco' => 77.53
            ],
            [
                'desc' => 'Teclado Logitech K120 Resistente à Água Preto ABNT2',
                'img' => '/images/prod5.jpg',
                'preco' => 59.88
            ],
            [
                'desc' => 'Mouse Logitech M90 Preto 1000DPI',
                'img' => '/images/prod6.jpg',
                'preco' => 29.29
            ]
        ];
        
        foreach ($produtos as $produto) {
            $p = new Produto();
            $p->setDescricao($produto->desc);
            $p->setImgpath($produto->img);
            $p->setValor($produto->preco);

            $manager->persist($p);
        }

        $manager->flush();
    }
}