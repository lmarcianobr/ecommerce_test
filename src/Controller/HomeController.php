<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * Home do eCommerce
     *
     * @Route("/", name="home")
     */
    public function homeAction () 
    {
        $this->handleSessionAttributes();

        return $this->render('home/home.html.twig');
    }

    /**
     * Retorna um JSON com 3 produtos para a seção de destaque
     *
     * @Route("/destaque", name="get_destaque", methods={"GET"})
     */
    public function getDestaques () 
    {
        $em = $this->getDoctrine()->getManager();

        $query = 'SELECT * FROM produto ORDER BY RAND() LIMIT 3;';

        $statement = $em->getConnection()->prepare($query);
        $statement->execute();

        $result = $statement->fetchAll();

        return new Response(json_encode($result));
    }

    /**
     * Define os atributos da sessão armazenada
     */
    public function handleSessionAttributes ()
    {
        if (is_null($this->get('session'))) {
            $session = new Session();
            
            $session->start();
            $session->set('qtdItens', 0);
            $session->set('itens', json_encode([]));
        } else {
            $session = $this->get('session');

            if (!$session->has('qtdItens')) {
                $session->set('qtdItens', 0);    
            }

            if (!$session->has('itens')) {
                $session->set('itens', json_encode([]));
            }
        }
    }
}