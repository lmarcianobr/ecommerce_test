<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProdutosController extends AbstractController
{
    /**
     * Pagina de Produtos
     *
     * @Route("/produtos", name="page_produtos")
     */
    public function produtosAction () 
    {
        $em = $this->getDoctrine()->getManager();
        $id = $this->get('session');

        $query = 'SELECT * FROM produto;';

        $statement = $em->getConnection()->prepare($query);
        $statement->execute();

        $result = $statement->fetchAll();

        return $this->render('produtos/produtos.html.twig', [
            'qtdItens' => 0,
            'itens' => $result, 'id' => $id
        ]);
    }
}