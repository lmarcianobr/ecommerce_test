<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ParameterBag;
use App\Entity\Produto;
use App\Entity\Pedido;
use App\Entity\PedidoItem;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CheckoutController extends AbstractController
{
    /**
     * Finalizar Pedido
     *
     * @Route("/checkout", name="page_checkout")
     */
    public function carrinhoAction () 
    {
        $carrinho = $this->getSessionAttributes();

        return $this->render('checkout/checkout.html.twig', [
            'carrinho' => $carrinho
        ]);
    }

    /**
     * Resumo do Pedido
     *
     * @Route("/resumo", name="page_resumo")
     */
    public function resumoAction (Request $request, \Swift_Mailer $mailer) 
    {
        $carrinho = $this->getSessionAttributes();
        $pedido = json_decode($request->request->get('dados-pedido'));

        $em = $this->getDoctrine()->getManager();

        // Colocando o pedido no banco de dados
        $ped = new Pedido();
        $ped->setNome($pedido->nome);
        $ped->setTelefone($pedido->telefone);
        $ped->setCep($pedido->cep);
        $ped->setEndereco($pedido->endereco);
        $ped->setNumero($pedido->numero);
        $ped->setBairro($pedido->bairro);
        $ped->setCidade($pedido->cidade);
        $ped->setUf($pedido->uf);
        $ped->setEmail($pedido->email);
        $ped->setFormaPgto($pedido->formapgto);

        foreach ($carrinho['itens'] as $item) {
            $pi = new PedidoItem();
            $produto = $this->getDoctrine()->getRepository(Produto::class)
                            ->find($item->id);
            $pi->setPedido($ped);
            $pi->setProduto($produto);
            $pi->setQuantidade($item->qtd);

            $ped->addItem($pi);

            $em->persist($pi);
        }

        $em->persist($ped);
        $em->flush();

        // Formata o CEP e o Telefone para exibição depois de inseridos na base
        $ped = $this->formataPedido($ped);

        $msg = (new \Swift_Message('Obrigado pela sua compra!'))
                ->setFrom('ectestlmarciano@gmail.com')
                ->setTo($pedido->email)
                ->setBody($this->renderView(
                        'checkout/email.html.twig', [
                            'carrinho' => $carrinho,
                            'pedido' => $ped
                        ]
                    ), 'text/html');

        $mailer->send($msg);

        $this->resetCarrinho();

        return $this->render('checkout/resumo.html.twig', [
            'carrinho' => $carrinho,
            'pedido' => $ped
        ]);
    }

    /**
     * Recupera o carrinho da sessão
     */
    public function getSessionAttributes ()
    {
        $session = $this->get('session');

        if (is_null($session)) {
            
            $session = new Session();
            
            $session->start();
            $session->set('qtdItens', 0);
            $session->set('itens', json_encode([]));

            $carrinho = [
                'qtdItens' => 0,
                'itens' => []
            ];

        } else {
            
            if (!$session->has('qtdItens')) {
                $session->set('qtdItens', 0);    
            }

            if (!$session->has('itens')) {
                $session->set('itens', json_encode([]));
            }

            $carrinho = [
                'qtdItens' => $session->get('qtdItens'),
                'itens' => json_decode($session->get('itens'))
            ];

        }

        return $carrinho;
    }

    /**
     * Formata os campos do pedido para exibição na tela - CEP e telefone
     */
    public function formataPedido (Pedido $pedido) {
        $pedido->setCep($this->formataCep($pedido->getCep()));
        $pedido->setTelefone($this->formataTel($pedido->getTelefone()));

        return $pedido;
    }

    /**
     * Formata uma string contendo um CEP
     */
    public function formataCep ($cep) {
        $output = '';

        if (strlen($cep) === 8) {
            $output = substr($cep, 0, 2) . '.' .
                      substr($cep, 2, 3) . '-' .
                      substr($cep, 5, 3);

            return $output;
        } else {
            return $cep;
        }
    }

    /**
     * Formata uma string contendo um telefone no formato (XX) [X]XXXX-XXXX
     */
    public function formataTel ($tel) {
        $output = '';
        $len = strlen($tel);

        // Telefone com 8 digitos
        if ($len === 10) {
            $output = '(' . substr($tel, 0, 2) . ') ' .
                      substr(2, 4) . '-' . substr(6, 4);
            
            return $output;
        // Telefone com 9 digitos
        } else if ($len === 11) {
            $output = '(' . substr($tel, 0, 2) . ') ' .
                      substr($tel, 2, 5) . '-' . substr($tel, 7, 4);
            
            return $output;
        // Nenhum dos casos, só retorna o que chegou
        } else {
            return $tel;
        }
    }

    /**
     * Reseta o carrinho, feito após o término da compra
     */
    public function resetCarrinho () {
        $session = $this->get('session');
        $session->set('qtdItens', 0);
        $session->set('itens', json_encode([]));
    }
}