<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CarrinhoController extends AbstractController
{
    /**
     * Carrinho de Compras
     *
     * @Route("/carrinho", name="page_carrinho")
     */
    public function carrinhoAction () 
    {
        $carrinho = $this->getSessionAttributes();

        return $this->render('carrinho/carrinho.html.twig', [
            'carrinho' => $carrinho
        ]);
    }

    /**
     * Adicionar item ao Carrinho
     *
     * @Route("/add_item", name="page_add_item")
     */
    public function addItemAction (Request $request) 
    {
        $produto = json_decode($request->request->get('produto'));
        $carrinho = $this->getSessionAttributes();

        $existe = false;

        foreach ($carrinho['itens'] as $item) {
            if ($item->id == $produto->id) {
                $existe = true;
                $item->qtd++;
            }
        }

        if (!$existe) {
            $produto->qtd = 1;
            $carrinho['qtdItens']++;
            array_push($carrinho['itens'], $produto);
        }

        $this->setSessionAttributes($carrinho);

        return new Response(json_encode([
            'status' => true,
            'qtdItens' => $carrinho['qtdItens']
        ]));
    }

    /**
     * Retorna um objeto JSON com as informações do carrinho
     * 
     * @Route("/get_carrinho", name="page_get_carrinho")
     */
    public function getCarrinhoJSON () {
        return new Response(json_encode($this->getSessionAttributes()));
    }

    /**
     * Salva de um objeto JSON as informações do carrinho no objeto de sessão
     * 
     * @Route("/set_carrinho", name="page_set_carrinho")
     */
    public function setCarrinho (Request $request) {
        $carrinho = [
            'qtdItens' => $request->request->get('qtdItens'),
            'itens' => json_decode($request->request->get('itens'))
        ];

        $this->setSessionAttributes($carrinho);

        if ($this->get('session')->has('itens') && $this->get('session')->has('itens')) {
            return new Response("Carrinho de compras salvo com sucesso.");
        }
    }

    /**
     * Salvar na sessão o carrinho de compras apos sua modificação
     */
    public function setSessionAttributes ($carrinho)
    {
        $session = $this->get('session');

        if (!is_null($session)) {
            $session->set('qtdItens', $carrinho['qtdItens']);
            $session->set('itens', json_encode($carrinho['itens']));
        }
    }

    /**
     * Recupera o carrinho da sessão
     */
    public function getSessionAttributes ()
    {
        $session = $this->get('session');

        if (is_null($session)) {
            
            $session = new Session();
            
            $session->start();
            $session->set('qtdItens', 0);
            $session->set('itens', json_encode([]));

            $carrinho = [
                'qtdItens' => 0,
                'itens' => []
            ];

        } else {
            
            if (!$session->has('qtdItens')) {
                $session->set('qtdItens', 0);    
            }

            if (!$session->has('itens')) {
                $session->set('itens', json_encode([]));
            }

            $carrinho = [
                'qtdItens' => $session->get('qtdItens'),
                'itens' => json_decode($session->get('itens'))
            ];

        }

        return $carrinho;
    }
}