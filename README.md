# Teste de E-Commerce

Uma aplicação da web representado uma loja virtual fictícia. Nela, o usuário escolhe alguns produtos, que são inseridos em um carrinho de compras e finaliza o pedido inserindo algumas informações. No final, um e-mail com o resumo do pedido e agradecimento é enviado.

![Captura do website](screenshot.png)

Para utilizar o website, pode-se adicionar produtos pela página inicial - por meio dos produtos em destaque - ou na página *Produtos* onde todos os produtos da base de dados são listados. Uma vez adicionado no carrinho de compras, este pode ser modificado. Ao finalizar a compra, serão requisitados os dados do destinatário e caso válidos, o e-mail é enviado e um resumo da compra mostrado.

# Sobre o Projeto

A base desta aplicação é um projeto do [Symfony](https://symfony.com/), utilizando back-end em PHP, servidor Apache2 e MySQL para banco de dados. Neste projeto, foi utilizado o padrão Model-View-Controller, onde os modelos são dados pelos arquivos em `src/Entity`, as controladoras em `src/Controllers` e as visões em `templates`, com arquivos HTML utilizando-se a *engine* de templates TWIG, que faz parte do Symfony.

Os arquivos JavaScript que são utilizados em cada página se encontram em `public/js`.

Ferramentas utilizadas nas páginas:

* Bootstrap 4
* jQuery
* Ícones FontAwesome
* VueJS
 * Plugin Vue The-Mask para máscaras em campos utilizando Vue
* SweetAlert

# Requisitos

* Apache2
```
sudo apt install apache2
```

* PHP 7.1 ou superior
```
sudo apt install php-pear php-fpm php-dev php-zip php-curl php-xmlrpc php-gd php-mysql php-mbstring php-xml libapache2-mod-php 
```

* MySQL server
```
sudo apt install mysql-server
```

* Git
```
sudo apt install git 
```

* cURL
```
sudo apt install curl
```

Uma vez o back-end instalado e funcionando, ainda são necessários os gerenciadores de pacote **composer** e **yarn**.

### Composer
Para instalar o composer, primeiro obtém-se o arquivo de instalação.
```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
```
Verifica-se a integridade do arquivo:
```
php -r "if (hash_file('sha384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
```
Saída caso a integridade seja confirmada:
```
Installer verified
```
E procede-se com a instalação:
```
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
```

### Yarn
Sequência de comandos para a configuração do repositório e instalação - válidos para Ubuntu e Debian:
```
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
```

# Preparando o Projeto

O projeto utiliza o usuário **teste** com senha **teste** para se conectar a base de dados **ecommerce**. Isto pode ser editado no arquivo **.env** na raiz do repositório, caso necessário.

Cria-se o usuário em questão conectando-se no servidor MySQL e executando
```
GRANT ALL PRIVILEGES ON *.* TO 'teste'@'localhost' IDENTIFIED BY 'teste';
```

O Symfony possui um servidor web embutido, o qual será utilizado para a execução do projeto. Primeiramente, entre na pasta do projeto, após cloná-lo pelo git.
```
git clone https://lmarcianobr@bitbucket.org/lmarcianobr/ecommerce_test.git
cd /ecommerce_test
```
E instale as dependências. Primeiramente o composer:
```
composer install
```
E então o yarn:
```
yarn install
yarn encore dev
```
Por fim, a criação da base de dados, tabelas e inserção dos dados dos produtos que aparecerão na loja:
```
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixtures:load
```
Uma vez sucedido, o seguinte comando inicia o servidor web e a aplicação estará acessível no endereço [http://localhost:8000/](http://localhost:8000/)
```
php bin/console server:run
```