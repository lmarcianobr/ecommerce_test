const formatter = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL',
    minimumFractionDigits: 2
});

Vue.component('produto', {
    props: ['prod'],
    template:
        `<div class="col-md-4">
        <div class="card mb-4 box-shadow">
            <div class="card-header" style="height: 225px; width: 100%; overflow: hidden;">
                <img class="card-img-top" :alt="prod.desc" :src="prod.imgpath" data-holder-rendered="true">
            </div>
            <div class="card-body d-flex flex-column">
                <p class="card-text" style="text-align: justify;">{{prod.descricao}}</p>
                <div class="mt-auto d-flex justify-content-between align-items-center">
                    <button type="button" class="btn btn-sm btn-outline-secondary" @click="adicionarCarrinho">Adicionar ao Carrinho</button>
                    <small class="text-muted">{{moneyFormat}}</small>
                </div>
            </div>
        </div>
    </div>`,
    computed: {
        moneyFormat: function () {
            return formatter.format(this.prod.valor);
        }
    },
    methods: {
        adicionarCarrinho: function () {
            var url = $('#page-produtos').data('add-item');
            $.post(url,
                {
                    produto: JSON.stringify(this.prod)
                },
                function (data) {
                    dados = JSON.parse(data);
                    
                    if (dados.status) {
                        $('#qtd-itens-carrinho').text(dados.qtdItens);
                        swal('Item adicionado ao carrinho.')
                    }
                }
            );
        }
    }
});

var vueProdutos = new Vue({
    el: '#page-produtos',
    delimiters: ['${', '}$'],
    data: {
    },
    mounted: function () {
    }
});