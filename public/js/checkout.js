var vueCheckout = new Vue({
	el: '#page-checkout',
    delimiters: ['${', '}$'],
    data: {
        pedido: {
            nome: '',
            cep: '',
            endereco: '',
            telefone: '',
            numero: '',
            bairro: '',
            cidade: '',
            uf: '',
            email: '',
            formapgto: 'D'
        },
        valido: {
            nome: true,
            cep: true,
            email: true,
        }
    },
    methods: {
        buscaCEP: function () {
            if (vueCheckout.pedido.cep !== '' && vueCheckout.pedido.cep.length === 8) {
                $.getJSON("https://viacep.com.br/ws/"+ vueCheckout.pedido.cep +"/json/", function(dados) {
                    // Se não houverem erros, preenche os campos
                    if (dados.erro === undefined) {
                        vueCheckout.pedido.endereco = dados.logradouro;
                        vueCheckout.pedido.bairro = dados.bairro;
                        vueCheckout.pedido.cidade = dados.localidade;
                        vueCheckout.pedido.uf = dados.uf;
                    } else {
                        swal('CEP inválido.');
                    }
                });
            }
        },
        enviarPedido: function (e) {
            var r = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

            this.valido.nome = this.pedido.nome !== '';
            this.valido.cep = this.pedido.cep !== '' && this.pedido.cep.length === 8;
            this.valido.email = r.test(this.pedido.email);

            if (this.valido.nome && this.valido.cep && this.valido.email) {
                $('#dados-pedido').val(JSON.stringify(vueCheckout.pedido));
                return true;
            }

            e.preventDefault();
        }
    }
})