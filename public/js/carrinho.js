const formatter = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL',
    minimumFractionDigits: 2
});

var vueCarrinho = new Vue({
    el: '#page-carrinho',
    delimiters: ['${', '}$'],
    data: {
        loaded: false,
        carrinho: {
            qtdItens: 0,
            itens: []
        }
    },
    created: function () {
        var url = $('#page-carrinho').data('get-carrinho');

        $.getJSON(url,
            function (data) { 
                vueCarrinho.carrinho = data;
                vueCarrinho.loaded = true;
                $('#page-carrinho').removeClass('d-none');
            }
        );
    },
    methods: {
        formatar: function (val) {
            return formatter.format(val);
        },
        remover: function (item) {
            var index = vueCarrinho.carrinho.itens.indexOf(item);

            if (index > -1) {
                vueCarrinho.carrinho.itens.splice(index, 1);
            }

            vueCarrinho.carrinho.qtdItens = vueCarrinho.carrinho.itens.length;
            $('#qtd-itens-carrinho').text(vueCarrinho.carrinho.qtdItens);
        },
        salvar: function (showAlert = true) {
            var url = $('#page-carrinho').data('set-carrinho');

            $.post(url,
                {
                    qtdItens: vueCarrinho.carrinho.qtdItens,
                    itens: JSON.stringify(vueCarrinho.carrinho.itens)
                },
                function (data) {
                    if (showAlert) {
                        swal(data);
                    }
                }
            );
        }
    },
    computed: {
        valorTotal: function () {
            var total = 0;

            if (this.carrinho.itens != null) {
                this.carrinho.itens.forEach(function (item) {
                    total = total + (item.qtd * item.valor);
                });
            }

            return formatter.format(total);
        }
    }
});